# CCASS

## Installation

```
git clone https://gitlab.com/KAIMAN0/ccass.git
```

Better create conda environment with python 3.8 version

```
cd ccass
conda create -n ccass_env python=3.8
conda activate ccass_env
pip install -r requirements.txt
```

**Or** use the global environment

```
cd ccass
pip install -r requirements.txt
```

## Run

```
python manage.py runserver
```
