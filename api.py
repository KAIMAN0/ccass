from bs4 import BeautifulSoup as BS
from datetime import datetime
import pandas as pd
import requests

CCASS_URL = 'https://www3.hkexnews.hk/sdw/search/searchsdw.aspx'


def get_trend(stock_code: str, input_date: datetime):
    req_headers = {'user-agent': 'Mozilla/5.0 AppleWebKit/537.36 Chrome/103.0.0.0 Safari/537.36'}

    req_data = {'__EVENTTARGET': 'btnSearch',
                '__EVENTARGUMENT': '',
                '__VIEWSTATE': '/wEPDwUKMTY0ODYwNTA0OWRkVb0Z6/twMzkjq6As1zABy65ev+E=',
                '__VIEWSTATEGENERATOR': 'A7B2BBE2',
                'today': datetime.today().strftime('%Y%m%d'),
                'sortBy': 'shareholding',
                'sortDirection': 'desc',
                'alertMsg': '',
                'txtShareholdingDate': input_date.strftime('%Y/%m/%d'),
                'txtStockCode': "%05i" % int(stock_code),
                'txtStockName': '',
                'txtParticipantID': '',
                'txtParticipantName': '',
                'txtSelPartID': ''}

    response = requests.post(CCASS_URL, data=req_data, headers=req_headers)
    soup = BS(response.content, 'html.parser')
    # ccass_column = [item.find('div', {'class': 'mobile-list-heading'}).text.replace(':', '') for item in soup.select('table tbody tr')[0].select('td')]
    ccass_column = ['ID', 'Name', 'Address', 'Shareholding', 'percent_of_total']
    ccass_data = []
    for items in soup.select('table tbody tr'):
        ccass_data.append([item.find('div', {'class': 'mobile-list-body'}).text for item in items.select('td')])
    ccass_df = pd.DataFrame(ccass_data, columns=ccass_column)
    ccass_df['Shareholding_float'] = ccass_df['Shareholding'].str.replace(',', '').astype(float)
    ccass_df = ccass_df.sort_values('Shareholding_float', ascending=False).reset_index(drop=True)
    return ccass_df


def get_transaction(stock_code: str, start_date: datetime, end_date: datetime, threshold: float = 10):
    df_start = get_trend(stock_code, start_date).drop(['Address', 'percent_of_total'], axis=1)
    df_end = get_trend(stock_code, end_date).drop(['Address', 'percent_of_total'], axis=1)
    suffix_start = '_start'
    suffix_end = '_end'
    df_result = df_end.merge(df_start, on=['ID', 'Name'], suffixes=(suffix_end, suffix_start), how='outer')
    df_result['delta'] = (df_result['Shareholding_float' + suffix_end] / df_result[
        'Shareholding_float' + suffix_start] - 1).round(4) * 100
    return df_result[df_result['delta'].abs() >= threshold].sort_values('Shareholding_float' + suffix_end,
                                                                        ascending=False)


if __name__ == '__main__':
    get_trend('00005', datetime(2022, 7, 16)).to_csv('q1.csv', index=None)
    get_transaction('00005', datetime(2022, 6, 16), datetime(2022, 7, 16), 1).to_csv('q2.csv', index=None)
