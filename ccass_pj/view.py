# -*- coding: utf-8 -*-
import os
import json
from django.shortcuts import render
from datetime import datetime, timedelta
from api import get_trend, get_transaction
# import pandas as pd

BASE = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
TODAY = (datetime.today()- timedelta(days=1)).strftime("%Y/%m/%d")


def demo_home(req):
    return render(None, "home.html")


def trend_plot(req):
    stock_code = "00001" if req.GET['txtStockCode'] == "" else "%05i" % int(req.GET['txtStockCode'])
    input_date = TODAY if req.GET['dateShareholdingDate'] == "" else req.GET['dateShareholdingDate'].replace("-", '/')
    input_date = datetime(*list(map(int, input_date.split('/'))))
    response_df = get_trend(stock_code, input_date).head(10)
    # response_df = pd.read_csv('q1.csv').head(10)
    json_records = response_df.to_json(orient='records')
    data = json.loads(json_records)
    context = {'d': data, 'code': 'Success1', 'stock_code1': stock_code, 'input_date': input_date}
    return render(req, "home.html", context)


def transaction_finder(req):
    stock_code = "00001" if req.GET['txtStockCode'] == "" else "%05i" % int(req.GET['txtStockCode'])
    start_date = TODAY if req.GET['dateStartDate'] == "" else req.GET['dateStartDate'].replace("-", '/')
    end_date = TODAY if req.GET['dateEndDate'] == "" else req.GET['dateEndDate'].replace("-", '/')
    threshold = 0.0 if req.GET['dateEndDate'] == "" else float(req.GET['threshold'])
    start_date = datetime(*list(map(int, start_date.split('/'))))
    end_date = datetime(*list(map(int, end_date.split('/'))))
    response_df = get_transaction(stock_code, start_date, end_date, threshold)
    # response_df = pd.read_csv('q2.csv').head(20)
    json_records = response_df.to_json(orient='records')
    data = json.loads(json_records)
    context = {'d': data, 'code': 'Success2', 'stock_code2': stock_code, 'start_date': start_date, 'end_date': end_date,
               'threshold': threshold}
    return render(req, "home.html", context)
